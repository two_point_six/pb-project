#ifndef FileOperations_h_INCLUDED
#define FileOperations_h_INCLUDED

#include <stdbool.h>

long int file_size(char*); 	// Returns size of the given file in bytes
long int file_size_lines(char*);	// Returns number of existing lines in file
char *line_content(char*, long int);	// Returns line of the given number.
								// If number is -1, returns last line in the file
char *line_id(char*, long int);	// Returns id of the given line.
							// If number is -1, returns last line id
void add_line(char*, char*, bool); 	// Appends line to the file. 
									// If bool is true, adds corresponing id
									// (Assumes first number in the last line is id)
void create_file(char*, char*);	// Creates or overwrites file with given string
void copy_file(char*, char*);	// Creates a copy of the file with a given name
bool file_exists(char*);	// Checks if file exists
bool id_exists(char*, char*);	// Checks whether a line with given id exists in the file
long int line_number_with_id(char*, char*);	// Returns number of the line with given id
											// Returns -1 if there's no line with given id

// Functions below use "LinkedLists.h"
void replace_line(char*, long int, char*); 	// Replaces content of the line. 
											// If line_number == -1, replaces last line
void remove_line(char*, long int);	// Removes line from the file. 
									// If line_number == -1, removes last line

#endif // FileOperations_h_INCLUDED

