#ifndef LinkedLists_h_INCLUDED
#define LinkedLists_h_INCLUDED

typedef struct Lists {
	char *content;
	struct Lists *next;
} List;
List *list_create(char*);
void list_destroy(List**);
void insert_at_begin(List**, char*);
void insert_at_end(List**, char*);
void remove_at_begin(List**);
void remove_at_end(List**);
void remove_at(List**, long int);
long int number_of_elements(List*);
List *next(List*);
char *content_at(List*, long int);
void change_content_at(List**, char*, long int);

#endif // LinkedLists_h_INCLUDED

