#include <stdio.h>
#include <stdlib.h>
#include "LinkedLists.h"

void list_init(List *self, char *content) {
	self->content = content;
	self->next = NULL;
}

List *list_create(char *content) {
	List *result = (List*)malloc(sizeof(List));
	list_init(result, content);
	return result;
}

void list_destroy(List **self) {
	*self = NULL;
}

void insert_at_begin(List **self, char *content) {
	List *result = list_create(content);
	result->next = *self;
	*self = result;
}

void insert_at_end(List **self, char *content) {
	if(*self == NULL) {
		*self = list_create(content);
		return;
	}
	List *temp = *self;
	while(temp->next != NULL) 
		temp = temp->next;
	temp->next = list_create(content);
}

void remove_at_begin(List **self) {
	if(*self != NULL)
		*self = (*self)->next;
}

void remove_at_end(List **self) {
	if(*self != NULL) {
		if((*self)->next == NULL) {
			*self = NULL;
			return;
		}
		List *temp = *self;
		while(temp->next->next != NULL)
			temp = temp->next;
		temp->next = NULL;
	}
}

void remove_at(List **self, long int at) {
	if(*self != NULL && at < number_of_elements(*self) && at >= -1) {
		List *temp = *self;
		if(at == -1) {
			while(temp->next->next != NULL) 
				temp = temp->next;
			temp->next = NULL;
			return;
		}
		for(int i = 0; i < at-1; i++) 
			temp = temp->next;
		if(temp->next->next == NULL) {
			temp->next = NULL;
			return;
		}
		List *rest = temp->next->next;
		temp->next = rest;
	} 
}

long int number_of_elements(List *self) {
	if(self == NULL)
		return 0;
	long int i = 1;
	while(self->next != NULL) {
		self = self->next;
		i++;
	}
	return i;
}

List *next(List *self) {
	return self->next;
}

char *content_at(List *self, long int at) {
	if(self == NULL)
		return NULL;
	List *temp = self;
	for(int i = 0; i < at; i++) {
		if(temp->next == NULL)
			return NULL;
		temp = temp->next;
	}
	return temp->content;
}

void change_content_at(List **self, char *content, long int at) {
	if(*self == NULL || at < -1)
		return;

	List *temp = *self;
	if(at == -1) {
		while(temp->next != NULL) {
			temp = temp->next;
		}
		temp->content = content;
		return;
	}
	for(int i = 0; i < at; i++) {
		if(temp->next == NULL)
			return;
		temp = temp->next;
	}
	temp->content = content;
}
