#include "Values.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "FileOperations.h"
#include "SafetyChecks.h"

bool valid_category(char *argv) {
	char *categories[7] = 	{
	        					"Electronics",
								"Home&Garden",
	            				"Fashion",
								"Kids",
	                			"Health",
								"Automotive",
	                    		"Sport",
							};
	for(int i = 0; i < 7; i++) {
		if(strcmp(argv, categories[i]) == 0)
			return true;
	}
	return false;
}
bool is_number(char *argv) {
	for(int i = 0; i < strlen(argv); i++) {
		if(argv[i] < 48 || argv[i] > 57)
			return false;
	}
	return true;
}
void check_username(char *argv) {
	if(argv[0] < 96 || argv[0] > 122) {
		if(argv[0] < 65 || argv[0] > 90) {
			printf("Username must start with a letter!\n");
			exit(0);
		}
	}
	if(argv[strlen(argv)-1] < 96 || argv[strlen(argv)-1] > 122) {
		if(argv[strlen(argv)-1] < 65 || argv[strlen(argv)-1] > 90) {
			printf("Username must end with a letter!\n");
			exit(0);
		}
	}
	if(strlen(argv) > 16 || strlen(argv) < 5) {
		printf("Username must be between 5-16 characters!\n");
		exit(0);
	}
}

int check_args(int argc, char *argv[]) {
	if(argc > 1) {
		if(strcmp(argv[1], "--show") == 0) {
			if(argc != 3) {
				printf("To show database content you have to provide needed information:\
						\ndatabase\n");
			}
			if(strcmp(argv[2], "listings") != 0 && strcmp(argv[2], "users") != 0) {
				printf("Invalid database type!\n");
				exit(0);
			}
			return arg_show;
		} else if(strcmp(argv[1], "--remove_all") == 0) { 
			if(argc != 3) {
				printf("To remove listings with owner_id you have to provide needed information:\
						\nowner_id\n");
			}
			return arg_remove_all;
		} else if(strcmp(argv[1], "--add") == 0) {
			if(argc != 6) {
				printf("To add new item for listing you have to provide needed information:\
						\n-owner_id\
						\n-name\
						\n-category\
						\n-price\n");
				exit(0);
			}
			if(strlen(argv[3]) > 24 || strlen(argv[3]) < 5) {
				printf("Listing name must contain between 5-24 characters!\n");
				exit(0);
			}
			if(!valid_category(argv[4])) {
				printf("Invalid category!\n");
				exit(0);
			}
			if(!is_number(argv[5])) {
				printf("Invalid price!\n");
				exit(0);
			}
			if(strlen(argv[5]) > 5) {
				printf("Price too big!\n");
				exit(0);
			}
			return arg_add;
		} else if(strcmp(argv[1], "--buy") == 0) {
			if(argc != 4) {
				printf("To buy an item you have to provide needed information:\
						\n-auction_id\
						\n-buyer_id\n");
				exit(0);
			}
			return arg_buy;
		} else if(strcmp(argv[1], "--edit") == 0) {
			if(argc != 5) {
				printf("To edit an item you have to provide needed information:\
						\n-auction_id\
						\n-field name\
						\n-new field value\n");
				exit(0);
			}
			if(strcmp(argv[3], "category") == 0) {
				if(!valid_category(argv[3])) {
					printf("Invalid category!\n");
					exit(0);
				}
			} else if(strcmp(argv[3], "price") == 0) {
				if(argv[4][0] == 48) {
					printf("Price can't start with 0!\n");
					exit(0);
				}
				if(!is_number(argv[4])) {
					printf("Price must be a number!\n");
					exit(0);
				}
				if(strlen(argv[4]) > 5) {
					printf("Price is too big!\n");
					exit(0);
				}
			} else if(strcmp(argv[3], "name") == 0) {
				if(strlen(argv[4]) > 24 || strlen(argv[4]) < 5) {
					printf("Listing name must contain between 5-24 characters!\n");
					exit(0);
				}
			} else {
				printf("Wrong field name!\n");
				exit(0);
			}
			return arg_edit;
		} else if(strcmp(argv[1], "--remove") == 0) {
			if(argc != 3) {
				printf("To remove an item you have to provide needed information:\
						\n-auction_id\n");
				exit(0);
			}
			return arg_remove;
		} else if(strcmp(argv[1], "--filter") == 0) {
			if(argc == 5 || argc == 7) {
				if(strcmp(argv[2], "price") != 0) {
					printf("Too many arguments!\n");
					exit(0);
				}
				if(argc == 5 && (strcmp(argv[3], "more") != 0 && strcmp(argv[3], "less") != 0)) {
					printf("Invalid arguments!\n");
					exit(0);
				} else if(argc == 5) {
					if(!is_number(argv[4])) {
						printf("Invalid arguments!\n");
						exit(0);
					}
					return arg_filter;
				} else if(argc == 7 && strcmp(argv[3], "more") != 0 && strcmp(argv[4], "less") != 0) {
					printf("Invalid arguments!\n");
					exit(0);
				} else if(argc == 7) {
					if(!is_number(argv[5]) || !is_number(argv[6])) {
						printf("Invalid arguments!\n");
						exit(0);
					}
					return arg_filter;
				}
			}
			if(argc != 4) {
				printf("To filter auction listings you have to provide needed information:\
						\n-specify field to filter\
						\n-specify specific value\n");
				exit(0);
			}
			return arg_filter;
		} else if(strcmp(argv[1], "--sort") == 0) {
		if(argc != 3) {
				printf("To sort auction listings you have to provide needed information:\
						\n-specify sorting method\n");
				exit(0);
			}
			return arg_sort;
		} else if(strcmp(argv[1], "--filter_sort") == 0) {
			if(argc == 6 || argc == 8) {
				if(strcmp(argv[2], "price") != 0) {
					printf("Too many arguments!\n");
					exit(0);
				}
				if(argc == 6 && (strcmp(argv[3], "more") != 0 && strcmp(argv[3], "less") != 0)) {
					printf("Invalid arguments!\n");
					exit(0);
				} else if(argc == 6) {
					if(!is_number(argv[4])) {
						printf("Invalid arguments!\n");
						exit(0);
					}
					return arg_filter_sort;
				} else if(argc == 8 && strcmp(argv[3], "more") != 0 && strcmp(argv[4], "less") != 0) {
					printf("Invalid arguments!\n");
					exit(0);
				} else if(argc == 8) {
					if(!is_number(argv[5]) || !is_number(argv[6])) {
						printf("Invalid arguments!\n");
						exit(0);
					}
					return arg_filter_sort;
				}
			}
			if(argc != 5) {
				printf("To filter and sort auction listings you have to provide needed information:\
						\n-specify field to filter\
						\n-specify specific value\
						\n-specify sorting method\n");
				exit(0);
			}
			return arg_filter_sort;
		} else if(strcmp(argv[1], "--add_user") == 0) {
			check_username(argv[2]);
			if(argc != 3) {
				printf("To add new user to the database you have to provide needed information:\
						\n-user username\n");
				exit(0);
			}
			return arg_add_user;
		} else if(strcmp(argv[1], "--remove_user") == 0) {
			if(argc != 3) {
				printf("To remove user from the database you have to provide needed information:\
						\n-user id\n");
				exit(0);
			}
			return arg_remove_user;
		} else if(strcmp(argv[1], "--edit_user") == 0) {
			if(argc != 4) {
				printf("To edit user's name you have to provide needed information:\
						\n-user_id\
						\n-new user's name\n");
				exit(0);
			}
			check_username(argv[3]);
			return arg_edit_user;
		} else if(strcmp(argv[1], "--filter_username") == 0) {
			if(argc != 3) {
				printf("To filter users by their username you have to provide needed information\
						\n-filter username value\n");
				exit(0);
			}
			return arg_filter_username;
		} else {
			printf("Invalid argument!\n");
			exit(0);
		}
	} else if(argc == 1) {
		printf("Missing arguments!\n");
		exit(0);
	}
	printf("Invalid number of arguments!\n");
	exit(0);
}

void check_files() {
	char db_line[] = "AUCTION_ID\tNAME\tCATEGORY\tPRICE\tSTATUS\tOWNER_ID\tBUYER_ID\n";
	char udb_line[] = "USER_ID\tNAME\n";

	if(file_exists("database.txt")) {
		char *header = line_content("database.txt", 1);
		if(strncmp(header, db_line, strlen(db_line)-1) != 0) {
			printf("Creating backup of database.txt...\n");
			copy_file("database.txt", "database backup.txt");
			create_file("database.txt", db_line);
		}
	} else {
		create_file("database.txt", db_line);
	}
	if(file_exists("userdatabase.txt")) {
		char *header = line_content("userdatabase.txt", 1);
		if(strncmp(header, udb_line, strlen(udb_line)-1) != 0) {
			printf("Creating backup of userdatabase.txt...\n");
			copy_file("userdatabase.txt", "userdatabase backup.txt");
			create_file("userdatabase.txt", udb_line);
		}
	} else {
		create_file("userdatabase.txt", udb_line);
	}
}
