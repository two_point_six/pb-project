#ifndef Values
#define Values

enum arguments {
	arg_show,
	arg_add,
	arg_buy,
	arg_edit,
	arg_remove,
	arg_filter,
	arg_sort,
	arg_filter_sort,
	arg_add_user,
	arg_remove_user,
	arg_edit_user,
	arg_filter_username,
	arg_remove_all,
};

#endif
