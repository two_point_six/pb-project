#include "ArgumentOperations.h"
#include "SafetyChecks.h"
#include "Values.h"
#include <stdlib.h>
#include <stdio.h>

int main1(int argc, char *argv[]) {
    check_files();

	switch(check_args(argc, argv)) {
		case arg_show:
			argument_show(argv);
			break;
		case arg_add:
			argument_add(argv);
			break;

		case arg_buy:
			argument_buy(argv);
			break;

		case arg_edit:
			argument_edit(argv);
			break;

		case arg_remove:
			argument_remove(argv);
			break;

		case arg_filter:
			argument_filter(argv);
			break;

		case arg_sort:
			argument_sort(argv);
			break;

		case arg_filter_sort:
			argument_filter_sort(argv);
			break;

		case arg_add_user:
			argument_add_user(argv);
			break;

		case arg_remove_user:
			argument_remove_user(argv);
			break;

		case arg_edit_user:
			argument_edit_user(argv);
			break;

		case arg_filter_username:
			argument_filter_user(argv);
			break;

		case arg_remove_all:
			argument_remove_all(argv);
			break;
	}
	return 0;
}

int main(int argc, char *argv[]) {
	if(argc < 2) {
		printf("Invalid number of arguments!\n");
		exit(0);
	}
	if(argv[1][0] != '-' && argv[1][1] != '-') {
		int times = atoi(argv[1]);
		if(times > 100) {
			printf("Multiplication number cannot be higher than 100\n");
			exit(0);
		}
		for(int i = 1; i < argc-1; i++) {
			argv[i] = argv[i+1];
		}
		for(int i = 0; i < times; i++) {
			main1(argc-1, argv);
		}
	} else 
		main1(argc, argv);
}
