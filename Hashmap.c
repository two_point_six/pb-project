#include <stdio.h>
#include "Hashmap.h"

int partition(Map *A, long int p, long int r) {
	long int x = A[r].value;
	long int i = p-1;
	for(long int j=p; j<r; j++) {
		if(A[j].value <= x) {
			i++;
			Map a = A[i];
			A[i] = A[j];
			A[j] = a;
		}
	}
	Map a = A[i+1];
	A[i+1] = A[r];
	A[r] = a;
	return i+1;
}

void map_quicksort(Map *A, long int p, long int r) {
	if(p<r) {
		long int q = partition(A, p, r);
		map_quicksort(A, p, q-1);
		map_quicksort(A, q+1, r);
	}
}

