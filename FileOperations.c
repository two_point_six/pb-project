#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "FileOperations.h"
#include "LinkedLists.h"

long int file_size(char *file_name) {
	FILE *fp = fopen(file_name, "r"); 
	if (fp == NULL) {  
		return -1; 
	} 
	fseek(fp, 0L, SEEK_END); 

	long int res = ftell(fp); 
	fclose(fp);

	return res; 
}

long int file_size_lines(char *file_name) {
	long int size = file_size(file_name);
	if(size >= -1 && size <= 1) 
		return 0;
	char *line = (char*)calloc(size, sizeof(char));
	char *result;
	FILE *fp = fopen(file_name, "r");
	long int number_of_lines = 0;
	for(;; number_of_lines++) {
		result = fgets(line, size, fp);
		if(result == NULL || line[0] == '\n')
			break;
	}
	
	fclose(fp);
	return number_of_lines;
}

char *line_content(char *file_name, long int number) {
	long int size = file_size(file_name);
	char *line = (char*)calloc(size, sizeof(char));
	char *result;
	FILE *fp = fopen(file_name, "r");

	long int lines = file_size_lines(file_name);
	if(lines == 1)
		number = 1;
	if(lines == 0 || lines == -1) 
		return "";

	if(number == -1) {
		char *temp = (char*)calloc(size, sizeof(char));
		result = fgets(line, size, fp);
		if(result != NULL) {
			for(;;) {
				result = fgets(temp, size, fp);
				if(result == NULL)
					break;
				line = temp;
			}
		}
		fclose(fp);
		return line;
	} else if(number > 0) {
		for(long int i=0; i < number; i++) {
			result = fgets(line, size, fp);
			if(result == NULL) {
				printf("There's no %ld line.\n", number);
				exit(0);
			}
		}
		fclose(fp);
		return line;
	} else {
		fclose(fp);
		printf("Wrong line number\n");
		exit(0);
	}
}

char *line_id(char *file_name, long int number) {
	int lines = file_size_lines(file_name);
	if(number > lines) 
		return "-1";
	char *line = line_content(file_name, number);
	char *id = (char*)calloc(strlen(line), sizeof(char));
	int long i = 0;
	while(line[i] != '\t' && line[i] != '\0') {
		id[i] = line[i];
		i++;
	}

	if(id[0] == 0)
		return "-1";

	return id;
}
void add_line(char *file_name, char *line, bool with_id) {
	if(with_id) {
		long int lines = file_size_lines(file_name);
		char *l_content = line_content(file_name, lines);
		char *id = (char*)calloc(strlen(line), sizeof(line));

		int size = 0;
		for(long int i=0; ; i++) {
			if(l_content[i] > 47 && l_content[i] < 58) {
				if(i == 0 && l_content[i] == 48)
					break;
				id[i] = l_content[i];
				size++;
			} else 
				break;
		}
		if(size == 0) {
			char* newline = (char*)calloc(5 + strlen(line), sizeof(char));
			snprintf(newline, strlen(line)+3, "1\t%s", line);
			add_line(file_name, newline, false);	
		} else {
			int int_id = atoi(id)+1;
			char *newline = (char*)calloc(strlen(id) + 4 + strlen(line), sizeof(char));
			snprintf(newline, strlen(line)+strlen(id)+4, "%d\t%s\n", int_id, line);
			add_line(file_name, newline, false);
		}
	}
	else {
		FILE *fp = fopen(file_name, "a");
		if(line[strlen(line)-1] != '\n')
			fprintf(fp, "%s\n", line);
		else
			fprintf(fp, "%s", line);
		fclose(fp);
	}

}

void create_file(char *file_name, char *content) {
	FILE *fp = fopen(file_name, "w");
	fprintf(fp, "%s", content);
	fclose(fp);
}

void copy_file(char *file_name, char *copy_name) {
	long int lines = file_size_lines(file_name);
	create_file(copy_name, "");
	for(long int i = 1; i <= lines; i++) {
		add_line(copy_name, line_content(file_name, i), false); 
	}
}

bool file_exists(char *file_name) {
	if(file_size_lines(file_name) == 0) 
		return false;
	FILE *fp = fopen(file_name, "r");
	if(fp != NULL) {
		fclose(fp);
		return true;
	}
	return false;
}

bool id_exists(char *file_name, char *id) {
	long int lines = file_size_lines(file_name);
	for(long int i = 1; i <= lines; i++) {
		if(strcmp(id, line_id(file_name, i)) == 0) {
			return true;
		}
	}
	return false;
}

long int line_number_with_id(char *file_name, char *id) {
	long int lines = file_size_lines(file_name);
	for(long int i = 1; i <= lines; i++) {
		if(strcmp(id, line_id(file_name, i)) == 0) {
			return i;
		}
	}
	return -1;
}

void replace_line(char *file_name, long int line_number, char *new_line) {
	if(!file_exists(file_name))
		return;
	long int number_of_lines = file_size_lines(file_name);
	if(line_number > number_of_lines || line_number < -1 || line_number == 0)
		return;

	List *lines = list_create(line_content(file_name, 1));
	for(long int i = 1; i < number_of_lines; i++)
		insert_at_end(&lines, line_content(file_name, i+1));

	if(line_number == -1)
		change_content_at(&lines, new_line, -1);
	else
		change_content_at(&lines, new_line, line_number-1);

	if(content_at(lines, 0)[strlen(content_at(lines, 0))-1] != '\n') {
		char *first_line = (char*)calloc(strlen(content_at(lines, 0))+2, sizeof(char));
		sprintf(first_line, "%s\n", content_at(lines, 0));
		create_file(file_name, first_line);
	} else 
		create_file(file_name, content_at(lines, 0));

	long int elements = number_of_elements(lines);

	for(long int i = 1; i < elements; i++) {
		add_line(file_name, content_at(lines, i), false);
	}
}

void remove_line(char *file_name, long int line_number) {
	if(!file_exists(file_name))
		return;
	long int number_of_lines = file_size_lines(file_name);
	if(line_number > number_of_lines || line_number < -1 || line_number == 0)
		return;

	List *lines = list_create(line_content(file_name, 1));
	for(long int i = 1; i < number_of_lines; i++)
		insert_at_end(&lines, line_content(file_name, i+1));

	remove_at(&lines, line_number-1);
	if(content_at(lines, 0)[strlen(content_at(lines, 0))-1] != '\n') {
		char *first_line = (char*)calloc(strlen(content_at(lines, 0))+2, sizeof(char));
		sprintf(first_line, "%s\n", content_at(lines, 0));
		create_file(file_name, first_line);
	} else 
		create_file(file_name, content_at(lines, 0));

	long int elements = number_of_elements(lines);

	for(long int i = 1; i < elements; i++) {
		add_line(file_name, content_at(lines, i), false);
	}
}
