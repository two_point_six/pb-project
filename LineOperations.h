#ifndef LineOperations_h_INCLUDED
#define LineOperations_h_INCLUDED

int number_of_fields(char*);
char *field(char*, int);
void replace_field(char**, int, char*);

#endif // LineOperations_h_INCLUDED

