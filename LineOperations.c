#include "LineOperations.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void double_append_to_string(char **str, char *middle, char *add) {
	char *result = (char*)calloc(strlen(*str)+strlen(middle)+strlen(add)+1, sizeof(char));
	sprintf(result, "%s%s%s", *str, middle, add);
	*str = result;
}

int number_of_fields(char *line) {
	if(strlen(line) <= 1)
		return 0;
	int j = 0;
	for(int i = 1; ; i++) {
		while(line[j] != '\t' && line[j] != '\n' && line[j] != 0) 
			j++;
		if(line[j] == '\n' || line[j] == 0) 
			return i;
		j++;
	}
}

char *field(char *line, int field_number) {
	if(number_of_fields(line) == 0 || field_number > number_of_fields(line))
		return NULL;
	int i = 0;
	for(int j = 1; j < field_number; j++) {
		while(line[i] != '\n' && line[i] != 0 && line[i] != '\t')
			i++;
		if(line[i] == '\n' || line[i] == 0)
			return NULL;
		i++;
	}
	char *result = (char*)calloc(strlen(line), sizeof(char));
	int j = 0;
	while(line[i] != '\n' && line[i] != 0 && line[i] != '\t') {
		result[j] = line[i];
		j++;
		i++;
	}
	return result;
}

void replace_field(char **line, int field_number, char *new_field) {
	int n_of_fields = number_of_fields(*line);
	if(field_number < -1 || field_number > n_of_fields || field_number == 0) 
		return;

	if(n_of_fields == 1) {
		double_append_to_string(&new_field, "\n", "");
		*line = new_field;
	} else if(field_number == 1) {
		char *part = (char*)calloc(strlen(*line)+strlen(new_field)+2, sizeof(char));
		double_append_to_string(&part, new_field, "\t");
		for(int i = 2; i < n_of_fields; i++) 
			double_append_to_string(&part, "\t", field(*line, i));
		double_append_to_string(&part, field(*line, n_of_fields), "\n");
		*line = part;
	} else if(field_number == -1 || field_number == n_of_fields) {
		char *part = (char*)calloc(strlen(*line)+strlen(new_field)+2, sizeof(char));
		for(int i = 1; i < n_of_fields; i++)
			double_append_to_string(&part, field(*line, i), "\t");
		double_append_to_string(&part, new_field, "\n");
		*line = part;
	} else {
		char *part = (char*)calloc(strlen(*line)+strlen(new_field)+2, sizeof(char));
		for(int i = 1; i < field_number; i++)
			double_append_to_string(&part, field(*line, i), "\t");
		double_append_to_string(&part, new_field, "\t");
		for(int i = field_number+1; i < n_of_fields; i++)
			double_append_to_string(&part, field(*line, i), "\t");
		double_append_to_string(&part, field(*line, n_of_fields), "\n");
		*line = part;
	}
}
