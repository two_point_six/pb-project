#ifndef ArgumentOperations
#define ArgumentOperations

void argument_show(char*[]);	// Displays database content
void argument_add(char*[]); // Adds new listing to the database
void argument_buy(char*[]);	// Changes listing status to "Bought" and adds BUYER_ID
void argument_add_user(char*[]);	// Adds user to the database
void argument_remove(char*[]);	// Removes auction listing from the database
void argument_remove_user(char*[]);	// Removes user from the database
void argument_remove_all(char*[]); // Removes all listings with corresponing OWNER_ID
void argument_edit(char*[]); // Edits specified field in the database
void argument_edit_user(char*[]);	// Edits user's name
void argument_filter(char*[]);	// Displays filtered database content
void argument_filter_user(char*[]); // Displays filtered user database content
void argument_sort(char*[]);	// Displays sorted database content
void argument_filter_sort(char*[]);	// Displays filtered and sorted database

#endif
