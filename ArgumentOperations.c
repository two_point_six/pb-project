#include "Values.h"
#include "FileOperations.h"
#include "ArgumentOperations.h"
#include "LineOperations.h"
#include "Hashmap.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

void argument_show(char *argv[]) {
	if(strcmp(argv[2], "listings") == 0) {
		long int number_of_lines = file_size_lines("database.txt");
		for(long int i = 1; i <= number_of_lines; i++)
			printf("%s", line_content("database.txt", i));
	} else if(strcmp(argv[2], "users") == 0) {
		long int number_of_lines = file_size_lines("userdatabase.txt");
		for(long int i = 1; i <= number_of_lines; i++)
			printf("%s", line_content("userdatabase.txt", i));
	} else {
		printf("Invalid database name!\n");
		exit(0);
	}
}

void argument_add(char *argv[]) {
	if(id_exists("userdatabase.txt", argv[2])) {
		int line_length = 6+10+4; // Special characters, "Not Bought", "None"
		line_length += strlen(argv[2]);
		line_length += strlen(argv[3]);
		line_length += strlen(argv[4]);
		line_length += strlen(argv[5]);


		char *line = (char*)calloc(line_length, sizeof(char));
		sprintf(line, "%s\t%s\t%s\tNot Bought\t%s\tNone", 
				argv[3], argv[4], argv[5], argv[2]);

		add_line("database.txt", line, true);
	} else {
		printf("No user with given id exists!\n");
		exit(0);
	}
}

void argument_buy(char *argv[]) {
	if(id_exists("database.txt", argv[2])) {
		if(!id_exists("userdatabase.txt", argv[3])) {
			printf("No user with given id exists!\n");
			exit(0);
		}
		long int line_number = line_number_with_id("database.txt", argv[2]);
		char *content = line_content("database.txt", line_number);
		if(strcmp(field(content, 5), "Bought") == 0) {
			printf("Auction is already bought!\n");
			exit(0);
		}
		replace_field(&content, 5, "Bought");
		replace_field(&content, 7, argv[3]);
		replace_line("database.txt", line_number, content);

	} else {
		printf("No listing with given id exists!\n");
		exit(0);
	}
}

void argument_add_user(char *argv[]) {
	add_line("userdatabase.txt", argv[2], true);
}

void argument_remove(char *argv[]) {
	if(!id_exists("database.txt", argv[2])) {
		printf("No listing with given id!\n");
		exit(0);
	}
	long int line_number = line_number_with_id("database.txt", argv[2]);
	remove_line("database.txt", line_number);
}

void argument_remove_user(char *argv[]) {
	if(!id_exists("userdatabase.txt", argv[2])) {
		printf("No user with given id!\n");
		exit(0);
	}
	long int line_number = line_number_with_id("userdatabase.txt", argv[2]);
	remove_line("userdatabase.txt", line_number);
}

void argument_remove_all(char *argv[]) {
	if(!id_exists("database.txt", argv[2])) {
		printf("No user with given id!\n");
		exit(0);
	}
	for(long int i = 2; ;) {
		if(strcmp(field(line_content("database.txt", i), 6), argv[2]) == 0) {
			remove_line("database.txt", i);
			if(i > file_size_lines("database.txt"))
				break;
		} else {
			i++;
		}
	}
}

void argument_edit(char *argv[]) {
	if(!id_exists("database.txt", argv[2])) {
		printf("No listing with given id!\n");
		exit(0);
	}
	long int line_number = line_number_with_id("database.txt", argv[2]);
	char *content = line_content("database.txt", line_number);

	if(strcmp(argv[3], "name") == 0) {
		replace_field(&content, 2, argv[4]);
	} else if(strcmp(argv[3], "category") == 0) {
		replace_field(&content, 3, argv[4]);
	} else if(strcmp(argv[3], "price") == 0) {
		replace_field(&content, 4, argv[4]);
	} else {
		printf("Wrong field name!\n");
		exit(0);
	}

	replace_line("database.txt", line_number, content);
}

void argument_edit_user(char *argv[]) {
	if(!id_exists("userdatabase.txt", argv[2])) {
		printf("No user with given id!\n");
		exit(0);
	}
	long int line_number = line_number_with_id("userdatabase.txt", argv[2]);
	char *content = line_content("userdatabase.txt", line_number);

	replace_field(&content, 2, argv[3]);
	replace_line("userdatabase.txt", line_number, content);
}

void argument_filter(char *argv[]) {
	long int line_number = file_size_lines("database.txt");
	printf("%s", line_content("database.txt", 1));
	if(strcmp(argv[2], "name") == 0) {
		for(long int i = 2; i <= line_number; i++) {
			if(strcmp(field(line_content("database.txt", i), 2), argv[3]) == 0)
				printf("%s", line_content("database.txt", i));
		}
	} else if(strcmp(argv[2], "category") == 0) {
		for(long int i = 2; i <= line_number; i++) {
			if(strcmp(field(line_content("database.txt", i), 3), argv[3]) == 0)
				printf("%s", line_content("database.txt", i));
		}
	} else if(strcmp(argv[2], "price") == 0) {
		if(strcmp(argv[3], "less") != 0 && strcmp(argv[3], "more") != 0) {
			for(long int i = 2; i <= line_number; i++) {
				if(strcmp(field(line_content("database.txt", i), 4), argv[3]) == 0)
					printf("%s", line_content("database.txt", i));
			}
		} else {
			char *temp;
			if(strcmp(argv[4], "less") == 0) {
				for(long int i = 2; i <= line_number; i++) {
					long int price = strtol(field(line_content("database.txt", i), 4), &temp, 10);
					if(price > strtol(argv[5], &temp, 10) && price < strtol(argv[6], &temp, 10))
						printf("%s", line_content("database.txt", i));
				}
			} else {
				for(long int i = 2; i <= line_number; i++) {
					long int price = strtol(field(line_content("database.txt", i), 4), &temp, 10);
					if(strcmp(argv[3], "less") == 0) {
						if(price < strtol(argv[4], &temp, 10))
							printf("%s", line_content("database.txt", i));
					} else {
						if(price > strtol(argv[4], &temp, 10))
							printf("%s", line_content("database.txt", i));
					}
				}
			}
		}
	} else if(strcmp(argv[2], "status") == 0) {
		for(long int i = 2; i <= line_number; i++) {
			if(strcmp(field(line_content("database.txt", i), 5), argv[3]) == 0)
				printf("%s", line_content("database.txt", i));
		}
	} else if(strcmp(argv[2], "owner_id") == 0) {
		for(long int i = 2; i <= line_number; i++) {
			if(strcmp(field(line_content("database.txt", i), 6), argv[3]) == 0)
				printf("%s", line_content("database.txt", i));
		}
	} else if(strcmp(argv[2], "buyer_id") == 0) {
		for(long int i = 2; i <= line_number; i++) {
			if(strcmp(field(line_content("database.txt", i), 7), argv[3]) == 0)
				printf("%s", line_content("database.txt", i));
		}
	} else {
		printf("Invalid field name!\n");
		exit(0);
	}
}

void argument_filter_user(char *argv[]) {
	long int line_number = file_size_lines("userdatabase.txt");
	printf("%s", line_content("userdatabase.txt", 1));
	for(long int i = 2; i <= line_number; i++) {
		if(strcmp(field(line_content("userdatabase.txt", i), 2), argv[2]) == 0) 
			printf("%s", line_content("userdatabase.txt", i));
	}
}

void argument_sort(char *argv[]) {
	if(strcmp(argv[2], "ascending") != 0 && strcmp(argv[2], "descending") != 0) {
		printf("Invalid arguments!\n");
		exit(0);
	}
	long int number_of_lines = file_size_lines("database.txt");
	Map *map = (Map*)calloc(number_of_lines-1, sizeof(Map));
	printf("%s", line_content("database.txt", 1));
	for(long int i = 2; i <= number_of_lines; i++) {
		map[i-2].key = i;
		map[i-2].value = atoi(field(line_content("database.txt", i), 4));
	}
	map_quicksort(map, 0, number_of_lines-2);
	if(strcmp(argv[2], "ascending") == 0) {
		for(long int i = 0; i < number_of_lines-1; i++) {
			printf("%s", line_content("database.txt", map[i].key));
		}
	} else if(strcmp(argv[2], "descending") == 0) {
		for(long int i = number_of_lines-2; i >= 0; i--) {
			printf("%s", line_content("database.txt", map[i].key));
		}
	}
}

void argument_filter_sort(char *argv[]) {
	long int line_number = file_size_lines("database.txt");
	printf("%s", line_content("database.txt", 1));
	Map *map = (Map*)calloc(line_number-1, sizeof(Map));
	long int j = 0;
	int k = 0;
	if(strcmp(argv[2], "name") == 0) {
		for(long int i = 2; i <= line_number; i++) {
			if(strcmp(field(line_content("database.txt", i), 2), argv[3]) == 0) {
				map[j].key = i;
				map[j].value = atoi(field(line_content("database.txt", i), 4));
				j++;
			}
		}
	} else if(strcmp(argv[2], "category") == 0) {
		for(long int i = 2; i <= line_number; i++) {
			if(strcmp(field(line_content("database.txt", i), 3), argv[3]) == 0) {
				map[j].key = i;
				map[j].value = atoi(field(line_content("database.txt", i), 4));
				j++;
			}
		}
	} else if(strcmp(argv[2], "price") == 0) {
		if(strcmp(argv[3], "less") != 0 && strcmp(argv[3], "more") != 0) {
			for(long int i = 2; i <= line_number; i++) {
				if(strcmp(field(line_content("database.txt", i), 4), argv[3]) == 0) {
					map[j].key = i;
					map[j].value = atoi(field(line_content("database.txt", i), 4));
					j++;
				}
			}
		} else {
			char *temp;
			if(strcmp(argv[4], "less") == 0) {
				k = 3;
				for(long int i = 2; i <= line_number; i++) {
					long int price = strtol(field(line_content("database.txt", i), 4), &temp, 10);
					if(price > strtol(argv[5], &temp, 10) && price < strtol(argv[6], &temp, 10)) {
						map[j].key = i;
						map[j].value = atoi(field(line_content("database.txt", i), 4));
						j++;
					}
				}
			} else {
				k = 1;
				for(long int i = 2; i <= line_number; i++) {
					long int price = strtol(field(line_content("database.txt", i), 4), &temp, 10);
					if(strcmp(argv[3], "less") == 0) {
						if(price < strtol(argv[4], &temp, 10)) {
							map[j].key = i;
							map[j].value = atoi(field(line_content("database.txt", i), 4));
							j++;
						}
					} else {
						if(price > strtol(argv[4], &temp, 10)) {
							map[j].key = i;
							map[j].value = atoi(field(line_content("database.txt", i), 4));
							j++;
						}
					}
				}
			}
		}
	} else if(strcmp(argv[2], "status") == 0) {
		for(long int i = 2; i <= line_number; i++) {
			if(strcmp(field(line_content("database.txt", i), 5), argv[3]) == 0) {
				map[j].key = i;
				map[j].value = atoi(field(line_content("database.txt", i), 4));
				j++;
			}
		}
	} else if(strcmp(argv[2], "owner_id") == 0) {
		for(long int i = 2; i <= line_number; i++) {
			if(strcmp(field(line_content("database.txt", i), 6), argv[3]) == 0) {
				map[j].key = i;
				map[j].value = atoi(field(line_content("database.txt", i), 4));
				j++;
			}
		}
	} else if(strcmp(argv[2], "buyer_id") == 0) {
		for(long int i = 2; i <= line_number; i++) {
			if(strcmp(field(line_content("database.txt", i), 7), argv[3]) == 0) {
				map[j].key = i;
				map[j].value = atoi(field(line_content("database.txt", i), 4));
				j++;
			}
		}
	} else {
		printf("Invalid field name!\n");
		exit(0);
	}
	map_quicksort(map, 0, j);
	if(strcmp(argv[4+k], "ascending") == 0) {
		for(long int i = 0; i <= j; i++) {
			if(map[i].key != 0)
				printf("%s", line_content("database.txt", map[i].key));
		}
	} else if(strcmp(argv[4+k], "descending") == 0) {
		for(long int i = j; i >= 0; i--) { 
			if(map[i].key != 0)
				printf("%s", line_content("database.txt", map[i].key));
		}
	}
}
